package kr.co.simplevod_view_tester.public_data;

import android.util.Log;

import java.util.ArrayList;

import kr.co.simplevod_view_tester.adapter.MainListAdapter;

public class DownloadProgressThreads {

    private static final String tag = "MYDEBUG";
    private static DownloadProgressThreads instance = null;
    private ArrayList<MainListAdapter.DownloadProgressUpdateThread> threads;
    private DownloadProgressThreads(){
        this.threads = new ArrayList<>();
    }
    public void pushThread(MainListAdapter.DownloadProgressUpdateThread thread){
        threads.add(thread);
    }
    public void stop(){
        for(int i = 0;i < threads.size(); ++i) {
            //threads.get(i).setDownloading(false);
            try {
                threads.get(i).sleep(1);
                threads.get(i).interrupt();
                if(threads.get(i).isInterrupted()){
                    Log.i(tag, "is interrupted");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
    public void restart(){
        for(int i = 0;i < threads.size(); ++i) {
            threads.get(i).setDownloading(true);

        }
    }
    public void clear(){
        for(int i = 0;i < threads.size(); ++i) {
            threads.get(i).setDownloading(false);
        }
        threads.clear();
    }
    public static DownloadProgressThreads getInstance() {
        if(instance == null) instance = new DownloadProgressThreads();
        return instance;
    }
}
