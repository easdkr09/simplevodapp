package kr.co.simplevod_view_tester.public_data;

import android.app.Activity;

import kr.co.simplevod_view_tester.VO.MVDataVO;

public class MVData_static {
    private static MVData_static instance = null;
    private Activity activity;
    private MVDataVO datas;

    private MVData_static(){

    }

    public static MVData_static getInstance(){
        if(instance == null) instance = new MVData_static();
        return instance;
    }

    public Activity getActivity(){
        return activity;
    }

    public void setActivity(Activity activity){
        this.activity = activity;
    }

    public MVDataVO getMusicVideoData(){
        return datas;
    }

    public void setMusicVideoData(MVDataVO musicVideoVO){
        this.datas = musicVideoVO;
    }
}
