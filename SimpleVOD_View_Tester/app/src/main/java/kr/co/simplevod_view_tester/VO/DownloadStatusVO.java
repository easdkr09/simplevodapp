package kr.co.simplevod_view_tester.VO;

public class DownloadStatusVO {
    public int videoId;
    public String downloadStatus;
    public String downloadLocation;
    public DownloadStatusVO(int videoId, String downloadStatus, String downloadLocation){
        this.videoId = videoId;
        this.downloadStatus = downloadStatus;
        this.downloadLocation = downloadLocation;
    }
}
