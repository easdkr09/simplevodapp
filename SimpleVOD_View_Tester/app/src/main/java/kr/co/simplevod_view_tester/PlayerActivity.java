package kr.co.simplevod_view_tester;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.io.IOException;

import kr.co.simplevod_view_tester.VO.MVDataVO;
import kr.co.simplevod_view_tester.public_data.DownloadStatus;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class PlayerActivity extends AppCompatActivity {
    private static final String BASE_URL = "http://203.233.111.177:3000";
    private static final String tag = "MYDEBUG";

    private PlayerView playerView;
    private SimpleExoPlayer player;
    private Boolean playerWhenReady = true;

    private int currentWindow = 0;
    private Long playbackPosition = 0L;

    private ImageView fullscreenButton;
    private boolean isFullscreen = false;

    private MVDataVO.SongData songData;

    private ImageView profileView;
    private ImageView coverView;
    private TextView lyricsText;
    private TextView releaseText;
    private TextView singerSongText;

    private String filePath;
    private int isDownloaded;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        //세로모드 고정
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        songData = (MVDataVO.SongData)getIntent().getSerializableExtra("song_data");
        isDownloaded = getIntent().getIntExtra("isDownloaded", -1);
        if(isDownloaded == DownloadStatus.DOWNLOADED)
            filePath = getIntent().getStringExtra("filePath");

        initViewId();
        setUI();
    }
    private void initViewId(){
        playerView = findViewById(R.id.exoPlayerView);
        fullscreenButton = findViewById(R.id.exo_fullscreen_icon);
        profileView = findViewById(R.id.player_profile_imageView);
        coverView = findViewById(R.id.player_album_cover_image);
        lyricsText = findViewById(R.id.player_lyrics_text);
        releaseText = findViewById(R.id.player_releaseDateText);
        singerSongText = findViewById(R.id.player_profile_albumText);
    }
    //각종 정보들 뷰에 세팅하기
    private void setUI(){
        Glide.with(this)
                .load(songData.profile)
                .into(profileView);
        Glide.with(this)
                .load(songData.albumCover)
                .into(coverView);
        //가사 데이터 가져오기
        new Thread(() -> {
            //가사 데이터를 가져오자
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url(songData.lyrics).build();
            //동기로 가져와
            try {
                Response response = client.newCall(request).execute();
                String lyrics = response.body().string();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        lyricsText.setText(lyrics);
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
        releaseText.setText(songData.albumRelease);
        singerSongText.setText(songData.singer + " - " + songData.songName);
    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.i(tag, "isDownloaded : " + isDownloaded);
        this.initPlayer();
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.releasePlayer();
    }

    private MediaSource buildMediaSource(Uri baseUri){
        String userAgent = Util.getUserAgent(this, getApplicationInfo().packageName);
        MediaItem mediaItem = MediaItem.fromUri(Uri.parse(BASE_URL + "/mv"));
        DefaultHttpDataSourceFactory factory =
                new DefaultHttpDataSourceFactory(userAgent, null);
        //스트리밍을 위한 헤더 설정 (key : value)
        factory.getDefaultRequestProperties().set("range", "bytes=0");
        factory.getDefaultRequestProperties().set("mv", songData.mv);
        ProgressiveMediaSource mediaSource =
                new ProgressiveMediaSource.Factory(factory)
                    .createMediaSource(mediaItem);
        return mediaSource;
    }
    private MediaSource buildMediaSourceFromLocalStorage(String path){
        String userAgent = Util.getUserAgent(this, getApplicationInfo().packageName);
        MediaItem mediaItem = MediaItem.fromUri(Uri.parse(path));
        DefaultDataSourceFactory factory =
                new DefaultDataSourceFactory(this);
        ProgressiveMediaSource mediaSource =
                new ProgressiveMediaSource.Factory(factory)
                        .createMediaSource(mediaItem);
        return mediaSource;
    }

    private void initPlayer(){
        if(player == null){
            player = new SimpleExoPlayer.Builder(this).build();
            //playerview와 simpleExoPlayer 연결
            playerView.setPlayer(player);
            //미디어 소스 세팅
            if(isDownloaded == DownloadStatus.DOWNLOADED) {
                Toast.makeText(this, "저장된 파일을 재생합니다.", Toast.LENGTH_SHORT).show();
                player.setMediaSource(buildMediaSourceFromLocalStorage(filePath));
            }
            else {
                Toast.makeText(this, "스트리밍 재생", Toast.LENGTH_SHORT).show();
                player.setMediaSource(buildMediaSource(Uri.parse(BASE_URL)));
            }
            //prepare
            player.prepare();
            player.setPlayWhenReady(playerWhenReady);

            fullscreenButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    if(isFullscreen){
                        fullscreenButton.setImageDrawable(ContextCompat
                                .getDrawable(PlayerActivity.this, R.drawable.ic_fullscreen_open));
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                        isFullscreen = false;
                    }else {
                        fullscreenButton.setImageDrawable(ContextCompat
                                .getDrawable(PlayerActivity.this, R.drawable.ic_fullscreen_close));
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                        isFullscreen = true;
                    }
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        if(getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_LANDSCAPE) {
            if(isFullscreen){
                fullscreenButton.setImageDrawable(ContextCompat
                        .getDrawable(PlayerActivity.this, R.drawable.ic_fullscreen_open));
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                isFullscreen = false;
            }
        }
        else
            this.finish();
    }

    private void releasePlayer(){
        if(player != null){
            playbackPosition = player.getCurrentPosition();
            currentWindow = player.getCurrentWindowIndex();
            playerWhenReady = player.getPlayWhenReady();
            playerView.setPlayer(null);
            player.release();
            player = null;
        }
    }
}
