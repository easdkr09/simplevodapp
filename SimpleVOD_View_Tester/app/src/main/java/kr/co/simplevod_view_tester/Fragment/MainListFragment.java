package kr.co.simplevod_view_tester.Fragment;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import kr.co.simplevod_view_tester.DB.VideoDownloadDatabaseHelper;
import kr.co.simplevod_view_tester.R;
import kr.co.simplevod_view_tester.VO.DownloadStatusVO;
import kr.co.simplevod_view_tester.VO.MVDataVO;
import kr.co.simplevod_view_tester.adapter.MainListAdapter;
import kr.co.simplevod_view_tester.httpcon.RetrofitAPI;
import kr.co.simplevod_view_tester.public_data.DownloadProgressThreads;
import kr.co.simplevod_view_tester.public_data.DownloadStatus;
import kr.co.simplevod_view_tester.public_data.MVData_static;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
public class MainListFragment extends Fragment {
    private static final String tag = "MYDEBUG";
    private static final String BASE_URL = "http://203.233.111.177:3000";
    private Context context;
    //레트로핏 관련
    private Retrofit retrofit;
    private RetrofitAPI retrofitAPI;
    private Call<MVDataVO> callMVList;

    //리사이클러뷰 관련
    private RecyclerView.LayoutManager layourManager;
    private RecyclerView recyclerView;
    private MainListAdapter adapter;
    //데이터를 담을 스태틱 객체
    private MVData_static mvdatas;
    //DB관련
    VideoDownloadDatabaseHelper videoDownloadDatabaseHelper;
    SQLiteDatabase db;
    public MainListFragment() {

    }


    @Override
    public void onResume() {
        DownloadProgressThreads.getInstance().restart();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        DownloadProgressThreads.getInstance().clear();
        Log.i(tag, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onStop() {
        DownloadProgressThreads.getInstance().stop();
        Log.i(tag, "onStop");
        super.onStop();
    }

    public static MainListFragment newInstance(String param1, String param2) {
        MainListFragment fragment = new MainListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        mvdatas = MVData_static.getInstance();
        setRetrofitInit();
        callMovieList();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.context = container.getContext();
        View root = inflater.inflate(R.layout.fragment_main_list, container, false);
        return root;
    }

    private void callMovieList(){
        callMVList = retrofitAPI.getMainList();
        callMVList.enqueue(retrofitCallback);
    }

    private final Callback<MVDataVO> retrofitCallback = new Callback<MVDataVO>() {
        @Override
        public void onResponse(Call<MVDataVO> call, Response<MVDataVO> response) {
            MVDataVO result = response.body();
            mvdatas.setMusicVideoData(result);
            initDownloadDatabase();
            //DB에 데이터가 있으면 가져와서 arraylist로 만들어준다.
            ArrayList<DownloadStatusVO> downloadStatusVOArray = new ArrayList<DownloadStatusVO>();
            Cursor cursor = videoDownloadDatabaseHelper.selectAll();
            if(!cursor.moveToFirst()){
                Log.i(tag, "DB Error");
                return;
            }

            while(cursor.moveToNext()){
                int videoId = cursor.getInt(cursor.getColumnIndexOrThrow(VideoDownloadDatabaseHelper.COL_VIDEO_ID));
                String downloadStatus = cursor.getString(cursor.getColumnIndexOrThrow(VideoDownloadDatabaseHelper.COL_DOWNLOAD_STATUS));
                String downloadLocation = cursor.getString(cursor.getColumnIndexOrThrow(VideoDownloadDatabaseHelper.COL_DOWNLOAD_LOCATION));
                downloadStatusVOArray.add(new DownloadStatusVO(videoId, downloadStatus, downloadLocation));
            }

            layourManager = new LinearLayoutManager(context);
            RecyclerView recyclerView = getView().findViewById(R.id.root);
            recyclerView.setLayoutManager(layourManager);
            adapter = new MainListAdapter(mvdatas.getMusicVideoData().datas, downloadStatusVOArray);
            Log.i(tag, "new adapter 생성 ");
            recyclerView.setAdapter(adapter);
        }

        @Override
        public void onFailure(Call<MVDataVO> call, Throwable throwable) {
            Log.i(tag,throwable.toString());
            throwable.printStackTrace();
        }
    };
    //최초 초기화
    //서버에서 가져온 데이터들을 로컬 db에 저장
    private void initDownloadDatabase(){
//        videoDownloadDatabaseHelper = new VideoDownloadDatabaseHelper(context);
//        videoDownloadDatabaseHelper.dropTable();
        videoDownloadDatabaseHelper = new VideoDownloadDatabaseHelper(context);
        if(!videoDownloadDatabaseHelper.selectAll().moveToFirst()){
            for(int i = 0; i < mvdatas.getMusicVideoData().datas.size(); ++i){
                String videoId = mvdatas.getMusicVideoData().datas.get(i).id + "";
                String downloadStatus = DownloadStatus.NOT_DOWNLOADED + "";
                String downloadLocation = "NULL";
                videoDownloadDatabaseHelper.insert(videoId, downloadStatus, downloadLocation);
            }
        }
    }
    private void setRetrofitInit(){
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        retrofitAPI = retrofit.create(RetrofitAPI.class);
    }
}