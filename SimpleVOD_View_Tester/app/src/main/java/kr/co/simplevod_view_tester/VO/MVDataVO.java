package kr.co.simplevod_view_tester.VO;

import java.io.Serializable;
import java.util.ArrayList;

public class MVDataVO implements Serializable {
    public ArrayList<SongData> datas;
    public class SongData implements Serializable{
        public int id;
        public String singer;
        public String profile;
        public String albumName;
        public String albumCover;
        public String albumRelease;
        public String lyrics;
        public String songName;
        public String thumbnail;
        public String mv;
    }
}
