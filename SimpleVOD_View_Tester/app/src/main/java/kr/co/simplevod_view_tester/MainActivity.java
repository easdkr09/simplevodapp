package kr.co.simplevod_view_tester;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import kr.co.simplevod_view_tester.Fragment.ArchiveFragment;
import kr.co.simplevod_view_tester.Fragment.MainListFragment;
import kr.co.simplevod_view_tester.Fragment.SearchFragment;
import kr.co.simplevod_view_tester.Fragment.ThumbUpFragment;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.os.Environment.MEDIA_MOUNTED;
import static android.os.Environment.MEDIA_MOUNTED_READ_ONLY;

public class MainActivity extends AppCompatActivity
implements BottomNavigationView.OnNavigationItemSelectedListener{
    private static final String tag = "MYDEBUG";

    BottomNavigationView bottomNavigationView;
    MainListFragment mainListFragment;
    SearchFragment searchFragment;
    ThumbUpFragment thumbUpFragment;
    ArchiveFragment archiveFragment;
    FragmentManager fragmentMng;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        //퍼미션 얻기
        String state = Environment.getExternalStorageState();
        if(state.equals(MEDIA_MOUNTED) || state.equals(MEDIA_MOUNTED_READ_ONLY))
            Log.i(tag, "Available to at least read");
        if(state.equals(MEDIA_MOUNTED))
            Log.i(tag,"Available to read and write");
        if(checkExternalStoragePermission()){
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE},
                    2
            );
        }
        initFragment();
    }

    private boolean checkExternalStoragePermission(){
        return (ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PERMISSION_GRANTED);
    }

    void initFragment(){
        this.mainListFragment = new MainListFragment();
        this.searchFragment = new SearchFragment();
        this.thumbUpFragment = new ThumbUpFragment();
        this.archiveFragment = new ArchiveFragment();
        fragmentMng = getSupportFragmentManager();
        fragmentMng
                .beginTransaction()
                .replace(R.id.main_layout, mainListFragment)
                .commitAllowingStateLoss();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.item_home){
            fragmentMng
                    .beginTransaction()
                    .replace(R.id.main_layout, mainListFragment)
                    .commitAllowingStateLoss();
            return true;
        } else if(item.getItemId() == R.id.item_search){
            fragmentMng
                    .beginTransaction()
                    .replace(R.id.main_layout, searchFragment)
                    .commitAllowingStateLoss();
            return true;
        } else if(item.getItemId() == R.id.item_archive){
            fragmentMng
                    .beginTransaction()
                    .replace(R.id.main_layout, archiveFragment)
                    .commitAllowingStateLoss();
            return true;
        } else if(item.getItemId() == R.id.item_thumbup){
            fragmentMng
                    .beginTransaction()
                    .replace(R.id.main_layout, thumbUpFragment)
                    .commitAllowingStateLoss();
            return true;
        }
        return false;
    }
}