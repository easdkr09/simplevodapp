 package kr.co.simplevod_view_tester.adapter;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.io.Serializable;
import java.util.ArrayList;

import kr.co.simplevod_view_tester.DB.VideoDownloadDatabaseHelper;
import kr.co.simplevod_view_tester.Fragment.DetailBottomSheetFragment;
import kr.co.simplevod_view_tester.PlayerActivity;
import kr.co.simplevod_view_tester.R;
import kr.co.simplevod_view_tester.VO.DownloadStatusVO;
import kr.co.simplevod_view_tester.VO.MVDataVO;
import kr.co.simplevod_view_tester.download.DownloadHelper;
import kr.co.simplevod_view_tester.public_data.DownloadProgressThreads;
import kr.co.simplevod_view_tester.public_data.DownloadStatus;

public class MainListAdapter extends RecyclerView.Adapter<MainListAdapter.ViewHolder> {
    static final String tag = "MYDEBUG";
    private static final String BASE_URL = "http://203.233.111.177:3000/";

    ArrayList<MVDataVO.SongData> mvData = null;
    ArrayList<DownloadStatusVO> downloadStatusVOS = null;
    DownloadStatus downloadStatus;
    DownloadHelper downloadHelper;
    VideoDownloadDatabaseHelper videoDownloadDatabaseHelper;
    String downloadedLocation;
    public DownloadProgressUpdateThread downloadProgressUpdateThread;
    //생성자에서 데이터 리스트 객체를 전달 받음
    public MainListAdapter(ArrayList<MVDataVO.SongData> mvlist, ArrayList<DownloadStatusVO> downloadStatusVOS){
        this.mvData = mvlist;
        this.downloadStatusVOS = downloadStatusVOS;
    }

    //아이템 뷰를 위한 뷰홀더 객체 생성하여 리턴
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.main_list_recyclerview_item, parent, false);
        MainListAdapter.ViewHolder vh = new MainListAdapter.ViewHolder(view);
        return vh;
    }

    //position에 해당하는 데이터를 뷰홀더의 아이템에 표시
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        int videoId = mvData.get(position).id;
        String profileUrl = mvData.get(position).profile;
        String thumbnailUrl = mvData.get(position).thumbnail;
        String albumRelease = mvData.get(position).albumRelease;
        String songName = mvData.get(position).songName;
        String singer = mvData.get(position).singer;
        downloadStatus = DownloadStatus.getInstance();

        //다운로드 헬퍼 객체 생성
        videoDownloadDatabaseHelper = new VideoDownloadDatabaseHelper(holder.itemView.getContext());
        //텍스트뷰 세팅
        holder.releaseDateText.setText(albumRelease);
        holder.singerSongText.setText(singer + " - " + songName);
        //이미지 파일 가져오기
        Glide.with(holder.itemView)
                .load(profileUrl)
                .into(holder.profileImage);
        Glide.with(holder.itemView)
                .load(thumbnailUrl)
                .into(holder.thumbnailImage);

        Cursor allDataCursor = videoDownloadDatabaseHelper.selectAll();
        if(!allDataCursor.moveToFirst())return;
        Log.i(tag, "----------------------------------");
        int _id = allDataCursor.getInt(allDataCursor.getColumnIndexOrThrow(VideoDownloadDatabaseHelper.COL_VIDEO_ID));
        String _dl = allDataCursor.getString(allDataCursor.getColumnIndexOrThrow(VideoDownloadDatabaseHelper.COL_DOWNLOAD_LOCATION));
        String _ds = allDataCursor.getString(allDataCursor.getColumnIndexOrThrow(VideoDownloadDatabaseHelper.COL_DOWNLOAD_STATUS));
        Log.i(tag, _id + "," + _dl + "," + _ds);
        while(allDataCursor.moveToNext()) {
            _id = allDataCursor.getInt(allDataCursor.getColumnIndexOrThrow(VideoDownloadDatabaseHelper.COL_VIDEO_ID));
            _dl = allDataCursor.getString(allDataCursor.getColumnIndexOrThrow(VideoDownloadDatabaseHelper.COL_DOWNLOAD_LOCATION));
            _ds = allDataCursor.getString(allDataCursor.getColumnIndexOrThrow(VideoDownloadDatabaseHelper.COL_DOWNLOAD_STATUS));
            Log.i(tag, _id + "," + _dl + "," + _ds);
        }
        Log.i(tag, "----------------------------------");
        //DB조회해서 다운로드된 영상은 다운로드 버튼 지우
        Cursor cursor = videoDownloadDatabaseHelper.selectById(videoId + "");
        //Log.i(tag, String.valueOf(videoId));
        if(!cursor.moveToFirst()) { return; }
        String downloadStatusFromDB = cursor.getString(cursor.getColumnIndexOrThrow(VideoDownloadDatabaseHelper.COL_DOWNLOAD_STATUS));
        if(Integer.parseInt(downloadStatusFromDB) == DownloadStatus.DOWNLOADED) {
            ((Activity) holder.itemView.getContext()).runOnUiThread(() -> holder.downloadButton.setVisibility(View.GONE));
        }
        else{
            ((Activity) holder.itemView.getContext()).runOnUiThread(() -> {
                holder.progressText.setVisibility(View.GONE);
                holder.downloadButton.setVisibility(View.VISIBLE);
            });
        }
        //다시 현재 Fragment로 돌아왔을 때, 다운로드 중인 작업이 있다면
        //Progress UI 업데이트해주는 thread 재실행
        if(downloadStatus.getDownloadProgressById(videoId) != -1){
            holder.downloadButton.setVisibility(View.GONE);
            holder.progressText.setVisibility(View.VISIBLE);
            downloadProgressUpdateThread = new DownloadProgressUpdateThread(videoId,holder);
            downloadProgressUpdateThread.start();
            DownloadProgressThreads.getInstance().pushThread(downloadProgressUpdateThread);
        }
        //썸네일 클릭 리스너  : playActivity 실행
        holder.thumbnailImage.setOnClickListener(v -> {
            //DB에서 videoId로 다운로드 되었는지 조회한다.
            Cursor downloadInfoCursor = getDownloadedInfo(videoId);
            String filePath = downloadInfoCursor.getString(downloadInfoCursor.getColumnIndexOrThrow(VideoDownloadDatabaseHelper.COL_DOWNLOAD_LOCATION));
            int isDownloaded = downloadInfoCursor.getInt(downloadInfoCursor.getColumnIndexOrThrow(VideoDownloadDatabaseHelper.COL_DOWNLOAD_STATUS));
            Intent playerIntent = new Intent(holder.itemView.getContext(), PlayerActivity.class);
            playerIntent.putExtra("isDownloaded", isDownloaded);
            playerIntent.putExtra("song_data", mvData.get(position));
            playerIntent.putExtra("filePath", filePath);
            holder.itemView.getContext().startActivity(playerIntent);
        });

        //썸네일 롱클릭 리스너 : 바텀 시트 show
        holder.thumbnailImage.setOnLongClickListener((View.OnLongClickListener) v -> {
            Context context = holder.itemView.getContext();
            FragmentManager fragmentManager = ((AppCompatActivity)context)
                    .getSupportFragmentManager();
            DetailBottomSheetFragment detailBottomSheetFragment =
                    new DetailBottomSheetFragment();
            //바텀 시트에 데이터 전달
            Bundle bundle = new Bundle();
            bundle.putSerializable("song_data", (Serializable) mvData.get(position));
            detailBottomSheetFragment.setArguments(bundle);
            detailBottomSheetFragment.show(fragmentManager,"ttt");
            return true;
        });

        //downloadButton click
        holder.downloadButton.setOnClickListener(v -> {
            String mv = mvData.get(position).mv;
            String downloadSourcePath = BASE_URL + mv.substring(2);
            String dirPath = "AlphaFilx";
            String filePath = mvData.get(position).songName + ".mp4";
            downloadHelper = new DownloadHelper(v.getContext(), videoId);
            downloadHelper.downloadVideo(downloadSourcePath, dirPath, filePath);
            downloadedLocation = downloadHelper.getDownloadLocation();
            downloadHelper.writeDownloadProgress();
            if(downloadStatus.isDownloadingById(videoId)) {
                holder.downloadButton.setVisibility(View.GONE);
                holder.progressText.setVisibility(View.VISIBLE);
                holder.progressText.setText("DownLoading... " + "0%");
                downloadProgressUpdateThread = new DownloadProgressUpdateThread(videoId, holder);
                downloadProgressUpdateThread.start();
                DownloadProgressThreads.getInstance().pushThread(downloadProgressUpdateThread);
            }
        });
    }
    //Download 정보 return
    private Cursor getDownloadedInfo(int videoId){
        Cursor cursor = videoDownloadDatabaseHelper.selectById(String.valueOf(videoId));
        if(!cursor.moveToFirst()) return null;
        return cursor;
    }

    @Override
    public int getItemCount() {
        return mvData.size();
    }

    public class DownloadProgressUpdateThread extends Thread{
        int videoId;
        ViewHolder viewHolder;
        boolean downloading;

        DownloadProgressUpdateThread(int videoId, ViewHolder viewHolder){
            this.videoId = videoId;
            this.viewHolder = viewHolder;
            this.downloading = true;
        }

        public void setDownloading(boolean downloading) {
            this.downloading = downloading;
        }

        @Override
        public void run() {
            while(downloading) {
                int downloadProgress = downloadStatus.getDownloadProgressById(videoId);
                //다운로드가 취소된 상황 : 다시 downloadButton 생성시킴
                if(downloadProgress == DownloadHelper.DOWNLOAD_CANCEL){
                    ((Activity) viewHolder.itemView.getContext()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            viewHolder.progressText.setVisibility(View.GONE);
                            viewHolder.downloadButton.setVisibility(View.VISIBLE);
                        }
                    });
                    downloadStatus.removeDownloadProgressById(videoId);
                    downloading = false;
                }
                //다운로드 완료
                else if(downloadProgress == 100 || downloadProgress == -1) {
                    ((Activity) viewHolder.itemView.getContext()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            viewHolder.progressText.setVisibility(View.GONE);
                        }
                    });
                    downloading = false;
                    Log.i(tag, downloadStatus.getDownloadLocationById(videoId)+ "");
                    videoDownloadDatabaseHelper.update(videoId + "",
                            DownloadStatus.DOWNLOADED + "",
                            downloadStatus.getDownloadLocationById(videoId));
                    downloadStatus.removeDownloadProgressById(videoId);
                    downloadStatus.removeDownloadLocationById(videoId);
                }
                //다운로드 진행중
                else if(downloadProgress < 100 && downloadProgress >= 0) {
                    ((Activity) viewHolder.itemView.getContext()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            viewHolder.progressText.setText("DownLoading... " + downloadProgress + "%");
                        }
                    });
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            Log.i(tag, "run end ");
        }
    }


    //뷰홀더 클래스
    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView thumbnailImage;
        ImageView profileImage;
        TextView singerSongText;
        TextView releaseDateText;
        TextView progressText;
        ImageView downloadButton;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            thumbnailImage = itemView.findViewById(R.id.main_list_thumbnail_image);
            profileImage = itemView.findViewById(R.id.profile_image);
            singerSongText = itemView.findViewById(R.id.main_list_singer_song_text);
            releaseDateText = itemView.findViewById(R.id.main_list_release_date_text);
            downloadButton = itemView.findViewById(R.id.main_list_download_button);
            progressText = itemView.findViewById(R.id.main_list_download_progress_text);
        }
    }
}
