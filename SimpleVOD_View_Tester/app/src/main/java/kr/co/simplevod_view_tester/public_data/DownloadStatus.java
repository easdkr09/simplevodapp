package kr.co.simplevod_view_tester.public_data;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

public class DownloadStatus {
    public static final int NOT_DOWNLOADED = 0;
    public static final int DOWNLOADED = 1;
    private static final String tag = "MYTAG";
    private static DownloadStatus instance;
    //<video id, download progrees>
    private volatile HashMap<Integer, Integer> progress;
    //<video id, download location>
    private HashMap<Integer, String> location;
    public HashMap<Integer, Integer> getProgress() {
        return progress;
    }

    public int getCount(){
        return progress.size();
    }

    public int getDownloadProgressById(int videoId){
        return progress.containsKey(videoId) ? progress.get(videoId).intValue() : -1;
    }
    public void setDownloadLocationById(int videoId, String downloadLocation){
        Log.i(tag, videoId + ", " + downloadLocation);
        if(location.containsKey(videoId))
            location.replace(videoId, downloadLocation);
        else
            location.put(videoId, downloadLocation);
    }
    public void removeDownloadLocationById(int videoId){
        location.remove(videoId);
    }

    public String getDownloadLocationById(int videoId){
        return location.get(videoId);
    }
    public boolean isDownloadingById(int videoId){
        return progress.containsKey(videoId);
    }
    public void setDownloadProgressById(int videoId, int progress_value){
        if(progress.containsKey(videoId))
            progress.replace(videoId, progress_value);
        else
            progress.put(videoId, progress_value);
    }

    public void removeDownloadProgressById(int videoId){
        progress.remove(videoId);
    }
    public boolean containsDownloadingProcess(int videoId){
        return progress.containsKey(videoId);
    }
    private DownloadStatus(){
        instance = null;
        this.progress = new HashMap<>();
        this.location = new HashMap<>();
    }

    public static DownloadStatus getInstance(){
        if(instance == null) instance = new DownloadStatus();
        return instance;
    }


}
