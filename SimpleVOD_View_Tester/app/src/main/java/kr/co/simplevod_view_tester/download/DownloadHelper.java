package kr.co.simplevod_view_tester.download;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.File;

import kr.co.simplevod_view_tester.PlayerActivity;
import kr.co.simplevod_view_tester.public_data.DownloadStatus;

public class DownloadHelper {
    private static final String tag = "MYDEBUG";
    public static final int DOWNLOAD_CANCEL = -100;

    private DownloadStatus downloadStatus;
    private Context context;
    private long downloadId = -1;
    private DownloadManager downloadManager;
    private int videoId;
    private String downloadLocation;
    public DownloadHelper(Context context, int videoId){
        this.downloadManager = (DownloadManager)context.getSystemService(Context.DOWNLOAD_SERVICE);
        this.downloadStatus = DownloadStatus.getInstance();
        this.videoId = videoId;
        this.context = context;

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        intentFilter.addAction(DownloadManager.ACTION_NOTIFICATION_CLICKED);
        context.registerReceiver(onDownloadComplete, intentFilter);
    }

    public String getDownloadLocation(){
        return this.downloadLocation;
    }
    public void downloadVideo(String downloadSourcePath, String dirPath, String fileName){
        File destFile = createFile(dirPath, fileName);
        try {
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(downloadSourcePath))
                    .setTitle(fileName)
                    .setDescription(fileName + "  downloading...")
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
                    .setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE)
                    .setDestinationUri(Uri.fromFile(destFile))
                    .setMimeType("video/mp4")
                    .setRequiresCharging(false)
                    .setAllowedOverMetered(true)
                    .setAllowedOverRoaming(false);
            this.downloadId = downloadManager.enqueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //boolean isDownloadSucceeded = writeDownloadProgress();
    }
    //프로그래스를 DownloadStatus에 쓰는 쓰레드 작업
    public void writeDownloadProgress(){
        if(this.downloadId == -1)
            return;
        downloadStatus.setDownloadLocationById(videoId, downloadLocation);
        downloadStatus.setDownloadProgressById(videoId, 0);
        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean downloading = true;
                while(downloading) {
                    DownloadManager.Query q = new DownloadManager.Query();
                    q.setFilterById(downloadId);
                    Cursor cursor = downloadManager.query(q);
                    //다운로드가 취소됨
                    if(!cursor.moveToFirst()) {
                        downloadStatus.setDownloadProgressById(videoId, DOWNLOAD_CANCEL);
                        break;
                    }
                    int bytes_downloaded = cursor.getInt(cursor
                            .getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                    int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
                    if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
                            == DownloadManager.STATUS_SUCCESSFUL) {
                        downloadStatus.removeDownloadProgressById(videoId);
                        downloading = false;
                    }
                    final int downloadProgress = (int) ((bytes_downloaded * 100l) / bytes_total);
                    downloadStatus.setDownloadProgressById(videoId, downloadProgress);
                    cursor.close();
                }
            }
        }).start();
    }
    //상태값 전달
    public String getStatus(long id){
        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(id);
        Cursor cursor = downloadManager.query(query);
        if(!cursor.moveToFirst()){
            Log.i(tag,"Empty row");
            return "Wrong downloadId";
        }

        int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
        int status = cursor.getInt(columnIndex);
        int columnReason = cursor.getColumnIndex(DownloadManager.COLUMN_REASON);
        Log.i(tag, "columnReason : " + columnReason);
        int reason = cursor.getInt(columnReason);
        String statusText = "";
        if(status == DownloadManager.STATUS_SUCCESSFUL)
            statusText = "Successful";
        else if(status == DownloadManager.STATUS_FAILED)
            statusText = "Failed : " + reason;
        else if(status == DownloadManager.STATUS_PENDING)
            statusText = "Pending";
        else if(status == DownloadManager.STATUS_RUNNING)
            statusText = "Running";
        else if(status == DownloadManager.STATUS_PAUSED)
            statusText = "Pause : " + reason;
        else
            statusText = "Unknown";
        return statusText;
    }
    public void cancleDownload(long id){
        if(id != -1L) downloadManager.remove(id);
    }
    //파일을 생성하고 File 객체를 반환한다.
    private File createFile(String dirPath, String fileName){
        String rootDir = Environment.getExternalStorageDirectory().getAbsolutePath();
        //디렉토리 생성
        File dir = new File(rootDir + "/" + dirPath);
        if(!dir.exists()){ dir.mkdir(); }
        //파일 생성
        File file = new File(dir, fileName);
        this.downloadLocation = file.getAbsolutePath();
        return file;
    }

    BroadcastReceiver onDownloadComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            int cnt = 0;
            if(DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(intent.getAction())){
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(id);
                Cursor cursor = downloadManager.query(query);
                if(!cursor.moveToFirst()) {
                    Toast.makeText(context, "Download cancelled", Toast.LENGTH_SHORT).show();
                    return;
                }
                int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                int status = cursor.getInt(columnIndex);
                if(status == DownloadManager.STATUS_SUCCESSFUL) {
                    Toast.makeText(context, "Download succeeded", Toast.LENGTH_SHORT).show();
                }
                else if(status == DownloadManager.STATUS_FAILED)
                    Toast.makeText(context, "Download failed", Toast.LENGTH_SHORT).show();
            }
            else if(DownloadManager.ACTION_NOTIFICATION_CLICKED.equals(intent.getAction()))
                Toast.makeText(context,"Notification clicked", Toast.LENGTH_SHORT).show();
        }
    };
}
