package kr.co.simplevod_view_tester.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.io.IOException;

import kr.co.simplevod_view_tester.PlayerActivity;
import kr.co.simplevod_view_tester.R;
import kr.co.simplevod_view_tester.VO.MVDataVO;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class DetailBottomSheetFragment extends BottomSheetDialogFragment {
    private static final String tag = "MYDEBUG";
    ImageView cover_image;
    TextView singer_song_text;
    TextView release_text;
    TextView lyrics_text;
    Button play_button;
    ImageView download_button;
    MVDataVO.SongData songData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.detail_bottom_sheet_layout,container,false);
        this.initViewId(v);
        Log.i(tag, "BottomSheet onCreateView!");
        if(getArguments() != null) {
            songData = (MVDataVO.SongData) getArguments().get("song_data");
            Glide.with(this).load(songData.albumCover).into(cover_image);
            singer_song_text.setText(songData.singer + " - " + songData.songName);
            release_text.setText(songData.albumRelease);

            //가사데이터 가져오기
            new Thread(() -> {
                //가사 데이터를 가져오자
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url(songData.lyrics).build();
                //동기로 가져와
                try {
                    Response response = client.newCall(request).execute();
                    String lyrics = response.body().string();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            lyrics_text.setText(lyrics);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();
        }
        else{
            Toast.makeText(this.getContext(), "NULL", Toast.LENGTH_SHORT).show();
        }
        play_button.setOnClickListener(this::play_button_click);
        return v;
    }

    private void play_button_click(View v1) {
        Intent playerIntent = new Intent(getContext(), PlayerActivity.class);
        playerIntent.putExtra("song_data", songData);
        getContext().startActivity(playerIntent);

    }
    private void initViewId(View v){
        this.cover_image = v.findViewById(R.id.detail_bottom_sheet_cover_image);
        this.singer_song_text = v.findViewById(R.id.detail_bottom_sheet_singer_song_text);
        this.release_text = v.findViewById(R.id.detail_bottom_sheet_release_text);
        this.lyrics_text = v.findViewById(R.id.detail_bottom_sheet_lyrics_text);
        this.play_button = v.findViewById(R.id.detail_bottom_sheet_play_button);
    }
}
