package kr.co.simplevod_view_tester.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

public class VideoDownloadDatabaseHelper extends SQLiteOpenHelper {
    private static final String tag = "MYDEBUG";
    public static final String DATABASE_NAME = "DB_VIDEO";
    public static final String TABLE_NAME = "TABLE_VIDEO_DOWNLOADED";
    public static final String COL_VIDEO_ID = "VIDEO_ID";
    public static final String COL_DOWNLOAD_STATUS = "DOWNLOAD_STATUS";
    public static final String COL_DOWNLOAD_LOCATION = "DOWNLOAD_LOCATION";

    public VideoDownloadDatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME +
                "(VIDEO_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "DOWNLOAD_STATUS INTEGER, " +
                "DOWNLOAD_LOCATION TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
    public void dropTable(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE " + TABLE_NAME);
    }
    public boolean insert(String id, String download_status, String download_location){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_VIDEO_ID, id);
        contentValues.put(COL_DOWNLOAD_STATUS, download_status);
        contentValues.put(COL_DOWNLOAD_LOCATION, download_location);
        long result = db.insert(TABLE_NAME, null, contentValues);
        return result != -1;
    }
    public Cursor selectById(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE VIDEO_ID = " + id,null);
        return res;
    }
    public Cursor selectAll(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        return res;
    }

    public boolean update(String id, String download_status, String download_location){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_VIDEO_ID, id);
        contentValues.put(COL_DOWNLOAD_STATUS, download_status);
        contentValues.put(COL_DOWNLOAD_LOCATION, download_location);
        db.update(TABLE_NAME, contentValues, "VIDEO_ID = ?", new String[]{id});
        return true;
    }

    public int delete(String id){
        SQLiteDatabase db= this.getWritableDatabase();
        int ret= db.delete(TABLE_NAME, "VIDEO_ID = ?", new String[]{id});
        return ret;
    }
}
