package kr.co.simplevod_view_tester.httpcon;

import kr.co.simplevod_view_tester.VO.MVDataVO;
import retrofit2.Call;
import retrofit2.http.GET;

public interface RetrofitAPI {
    @GET("/getList/all")
    Call<MVDataVO> getMainList();
}
