const express = require("express");
const router = express.Router();
const fs = require("fs");
const { restart } = require("nodemon");

router.get('/', (req, res)=>{
    const CHUNK_SIZE = 10 ** 6; //1MB
    const range = req.headers.range;
    const mvUrl = req.headers.mv;
    console.log(req.headers)
    if(range && mvUrl){
        const parts = range.replace(/bytes=/, '').split('-');
        const videoSize = fs.statSync(mvUrl).size;
        const start = Number(parts[0]);
        const end = Math.min(start+CHUNK_SIZE, videoSize - 1);
        const contentLength = end-start+1;
        const header = {
            "Content-Range": `bytes ${start}-${end}/${videoSize}`,
            "Accept-Ranges": "bytes",
            "Content-Length": contentLength,
            "Content-Type": "video/mp4",
        }
        res.writeHead(206, header);
        const videoStream = fs.createReadStream(mvUrl, {start, end});
        videoStream.pipe(res);
    }
    else{
        res.status(400).send("Requires range header or mv header");
    }
});

module.exports = router;