const { json } = require('body-parser');
const { Console } = require('console');
const express = require('express');
const router = express.Router();
const fs = require('fs');

router.get('/all', (req, res)=>{
    const jsonFile = fs.readFileSync('./Datas/MV_DATA.json', 'utf8');
    const jsonData = JSON.parse(jsonFile);
    console.log(jsonData.datas.length);
    res.send(jsonData);
});

module.exports = router;