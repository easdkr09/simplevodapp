//모듈 정의
const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const fs = require('fs');
const static = require('serve-static');
const path = require('path');
const requestIp = require('request-ip');

const app = express();
const port = 3000;
//라우터 정의
var sendListRouter = require('./router/sendList');
var streamRouter = require('./router/streamingRouter');

//라우팅
app.use("/getList", sendListRouter);
app.use("/mv", streamRouter);

//스태틱 폴더 정의
app.use('/static', static(path.join(__dirname, 'static')));

// //클라이언트 아이피 확인 
// app.use('/', function(req, res){
//     console.log('client ip : ' + requestIp.getClientIp(req));
//     res.send('this is main page');
// });
app.listen(port, ()=>{
    console.log(`http://203.233.111.177:${port}...listen`);
});
